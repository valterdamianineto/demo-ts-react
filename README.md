# React demo components with Typescript

Creation of different components, demonstrating principles used in React with Typescript.


## Available Scripts

In the project directory, you can run:


### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


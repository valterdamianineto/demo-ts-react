import './App.css';
import Family from './components/basic/Family';
import FamilyMember from './components/basic/FamilyMember';
import FamilyV2 from './components/basic/FamilyV2';
import First from './components/basic/First';
import Params from './components/basic/Params';
import Randon from './components/basic/Randon';
import DirectFather from './components/communication/DirectFather';
import IndirectFather from './components/communication/IndirectFather';
import ConditionalList from './components/conditional/ConditionalList';
import ContInput from './components/controlled/Input';
import Counter from './components/counter/Counter';
import UserList from './components/iteration/UserList';
import Card from './components/layout/Card';
import NumberGenerator from './components/numberGenerator/NumberGenerator';
import users from './data/user';


export default function App() {
    return (
        <div className='app'>
            <h1>React Fundamentals</h1>
            <div className="cards">
                <Card title="Generator Component Example" borderColor='#2E5266' bgColor='#6E8898' contentColor='#2E5266'>
                    <NumberGenerator />
                </Card>
                <Card title="Controlled Component Example" borderColor='#2E5266' bgColor='#6E8898' contentColor='#2E5266'>
                    <Counter initialNumber={10} interval={5}/>
                </Card>
                <Card title="Controlled Component Example" borderColor='#2E5266' bgColor='#6E8898' contentColor='#2E5266'>
                    <ContInput/>
                </Card>
                <Card title="Direct Communication Example" borderColor='#2E5266' bgColor='#6E8898' contentColor='#2E5266'>
                    <IndirectFather/>
                </Card>
                <Card title="Indirect Communication Example" borderColor='#2D3142' bgColor='#4F5D75' contentColor='#2D3142'>
                    <DirectFather/>
                </Card>
                <Card title="Iteration Example" borderColor='#2D3142' bgColor='#4F5D75' contentColor='#2D3142'>
                    <ConditionalList usersList={users}/>
                </Card>
                <Card title="Iteration Example" borderColor='#B3E9C7' bgColor='#C2F8CB' contentColor='#B3E9C7'>
                    <UserList />
                </Card>
                <Card title="Relation Card Example" borderColor='#FF9F1C' bgColor='#FFBF69' contentColor='#FF9F1C'>
                    <FamilyV2 surname='Caffrey'>
                        <FamilyMember name='Neal'/>
                        <FamilyMember name='Peter'/>
                        <FamilyMember name='Elizabeth'/>
                    </FamilyV2>
                </Card>
                <Card title="Relation Card Example" borderColor='#028090' bgColor='#64B6AC' contentColor='#028090'>
                    <Family surname="Lavoisier"/>
                </Card>
                <Card title="Random Number" borderColor='#363457' bgColor='#735290' contentColor='#363457'>
                    <Randon min={10} max={100}/>
                </Card>
                <Card title="Card with Props Example" borderColor='#3D3B3C' bgColor='#7F7979' contentColor='#3D3B3C'>
                    <Params title="Title Example" subtitle="Subtitle Example"/>
                </Card>
                <Card title="Simple Component Example" borderColor='#2E5266' bgColor='#6E8898' contentColor='#2E5266'>
                    <First/>
                </Card>
            </div>
        </div>
    )
}
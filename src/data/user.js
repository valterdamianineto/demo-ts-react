export default [
    {id: "a1", name: "arthur", surname: "weasley", securityLevel: "confidential", accessCode: "749925cba3726ce57f38fa7beee9e6b2"},
    {id: "b2", name: "peter", surname: "burke", securityLevel: "top secret", accessCode: "b4c0874cb100b27cea480bea42acb145"},
    {id: "c3", name: "neal", surname: "caffrey", securityLevel: "secret", accessCode: "ae3544b6eb588d862cc876a46aef1edb"},
    {id: "d4", name: "alex", surname: "hunter", securityLevel: "confidential", accessCode: "92f77c79e87beb79e30c68c18e2f6616"},
    {id: "e5", name: "kate", surname: "moreau", securityLevel: "confidential", accessCode: "55c55e5a79b4fa49b84149aee940ae0b"},
    {id: "f6", name: "matthew", surname: "keller", securityLevel: "confidential", accessCode: "294ab9b1a339195cdb7d5f540d632463"},
    {id: "g7", name: "Vincent", surname: "adler", securityLevel: "confidential", accessCode: "b51bb2416eb32a1ab187c4bcb70dbdcf"},
    {id: "h8", name: "clinton", surname: "jones", securityLevel: "secret", accessCode: "5a4f893346a82c63134725b458e123c8"},
    {id: "i9", name: "reese", surname: "hughes", securityLevel: "top secret", accessCode: "40e8dbd4427191aa5abe604c02bba948"},
    {id: "j0", name: "phillip", surname: "kramer", securityLevel: "confidential", accessCode: "e6224dab1b52289c1f2433e30dd53c29"}
]
import { useState } from "react";
import './Input.css';

interface Props {

}

export default (props: Props) => {

    const [inputValue, setValue] = useState('')

    function handleChange(e: any) {
        setValue(e.target.value);
    }
    
    return (
        <div className="controlled-input">
            <input value={inputValue} onChange={handleChange}/>
            <br />
            <input value={inputValue} readOnly/>
            <br />
            <input value={inputValue} readOnly/>
        </div>
    )
}
import { useState } from "react";
import Controller from "./Controller";
import Generator from "./Generator";

export default () => {
    const [pairs, setPairs] = useState(0)
    const [showError, setShowError] = useState(false);
    const [newList, setList] = useState<Array<number>>(Array(pairs).fill(0));
    
    function generatePairs() {
        const min: number = 10
        const max: number = 60
        
        const numberList: Array<number> = Array(pairs).fill(0).reduce((nums) => {
            const newNumber: number = generateNumber(min, max, nums)
            return [...nums, newNumber]
        }, []);

        if(!showError) {
            setList(numberList)
        }
    }

    function generateNumber(min: number, max: number, list: any): number {
        let generatedNumber = Math.floor(Math.random() * (max + 1 - min) + min)
        
        return list.includes(generatedNumber) ? generateNumber(min, max, list) :  generatedNumber
    }

    function validate(value: any) {
        if(value <= 8 && value > 0) {
            setPairs(value)
            return setShowError(false)
        }
        setPairs(value)
        return setShowError(true);
    }

    return (
        <div>
            <h1>Number Generator</h1>
            <Generator list={newList}/>
            <Controller 
                showError={showError}
                onGenerate={generatePairs}
                validate={validate} 
                currentPairs={pairs}/>
        </div>
    )
}
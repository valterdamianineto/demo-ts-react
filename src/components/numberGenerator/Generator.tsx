interface Props {
    list: Array<number>;
}


export default (props: Props) => {
    const GeneratedNumber = props.list.map((item, index) => {
        
        return (
            <span className="show-number" key={index}>{item}</span>
        )
    })
    
    return (
        <div className="show-number-box">
            {GeneratedNumber}
        </div>
    )
}
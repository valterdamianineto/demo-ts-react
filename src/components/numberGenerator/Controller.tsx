import './Generator.css';
interface Props {
    onGenerate: () => void;
    validate: (e: any) => void;
    currentPairs: number;
    showError: boolean;
}

export default (props: Props) => {
    return (
        <div className="controller">
            <span>Pairs</span>
            <input type="number" name="generate" id="genereateInput" onChange={e => props.validate(+e.target.value)} value={props.currentPairs} />
            <button onClick={props.onGenerate}>Generate</button>
            <br />
            {props.showError ? 
                <span className='invalid-msg'>Invalid Value!</span>
             : 
                ''
            }
        </div>
    )
}
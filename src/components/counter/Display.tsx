interface Props {
    countNumber: number;
}

export default (props: Props) => {
    return (
        <div>
            <h3>{props.countNumber}</h3>
        </div>
    )
}
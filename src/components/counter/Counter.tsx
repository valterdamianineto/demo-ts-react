import React from "react";
import Buttons from "./Buttons";
import './Counter.css';
import Display from "./Display";
import Form from "./Form";

interface Props  {
    initialNumber: number;
    interval?: number;
}

interface State {
    countNumber: number;
    interval: number;
}

export default class Counter extends React.Component<Props> {

    state: State = {
        countNumber: this.props.initialNumber || 0,
        interval: this.props.interval || 1,
    }

    increment = () => {
        this.setState({
            countNumber: this.state.countNumber + this.state.interval,
        })
    }

    decrement = () => {
        this.setState({
            countNumber: this.state.countNumber - this.state.interval,
        })
    }

    setInterval = (newInterval: number) => {
        this.setState({
            interval: newInterval,
        })
    }
    

    render() {
        return (
        <div>
            <h1>Counter</h1>
            <Display countNumber={this.state.countNumber} />
            <Buttons onIncrement={this.increment} onDecrement={this.decrement} />
            <Form interval={this.state.interval} setInterval={this.setInterval}/>
        </div>
        );
    }
}

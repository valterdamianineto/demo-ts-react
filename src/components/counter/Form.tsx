interface Props {
    interval: number;
    setInterval: (e: any) => void;
}

export default (props: Props) => {
    return (
        <div className="form-interval">
            <label htmlFor="setInterval">Interval: </label>
            <input type="number" value={props.interval} onChange={e => props.setInterval(+e.target.value)}/>
        </div>
    )
}
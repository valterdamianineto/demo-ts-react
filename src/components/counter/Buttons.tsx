interface Props {
    onIncrement: () => void;
    onDecrement: () => void;
}

export default (props: Props) => {
    return (
        <div className="button-container">
            <button onClick={props.onIncrement}>+</button>
            <button onClick={props.onDecrement}>-</button>
        </div>
    )
}
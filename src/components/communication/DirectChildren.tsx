interface Props {
    text: string;
    number: number;
    bool: boolean;
}

export default (props: Props) => {
    return (
        <tr>
            <td>{props.text}</td>
            <td>{props.number}</td>
            <td>{props.bool ? 'True' : 'False'}</td>
        </tr>
    )
}
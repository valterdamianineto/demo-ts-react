import './Communication.css'

interface Props {
    getInfo: (name: string, age: number) => void
}

export default (props: Props) => {

    const nameList = ['Peter', 'Lisa', 'Luck', 'Mike', 'John']
    const getName = () => {
        let listIndex = (Math.floor(Math.random() * (4 - 0) + 1))
        return nameList[listIndex]
    }
    
    return (
        <div className="line">
            <div className='left'>
                <p>Get name and age</p>
            </div>
            <div className='right'>
                <button onClick={() => {
                    props.getInfo(getName(), Math.floor(Math.random() * (100 - 1) + 1))
                }}>Get Info
                </button>
            </div>
        </div>
    )
}
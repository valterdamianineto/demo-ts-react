import { useState } from 'react';
import Children from "./IndirectChildren";

export default () => {
    let [name, setName] = useState<string>('')
    let [age, setAge] = useState<number>(0)

    function handleInfo(name: string, age: number) {
        setName(name)
        setAge(age)
    }

    return (
        <div>
            <Children getInfo={handleInfo} />
            <div>
                {name ?  
                    <>
                    <span>The name is {name}</span> and the age are <span>{age}</span> 
                    </>
                    : ""
                }
            </div>
        </div>
    )
}
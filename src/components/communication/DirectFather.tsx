import Children from "./DirectChildren"

export default () => {
    return (
        <table>
            <tbody>
                <Children text="My text" number={2} bool={true} />
                <Children text="My text 2" number={5} bool={false} />
                <Children text="My text 3" number={8} bool={true} />
            </tbody>
        </table>
    )
}
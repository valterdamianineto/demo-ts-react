import './ConditionalList.css';
import Validation from './Validation';

interface Props {
    usersList: UserList[]
}

interface UserList {
    id?: string, 
    name?: string, 
    surname?: string, 
    securityLevel?: string, 
    accessCode?: string,
}

export default (props: Props) => {
        const userCode = props.usersList.map((user, index) => {
        const showCode = user.securityLevel === 'confidential' ? true : false;

        return (
            <tr key={user.id} className={index % 2 === 0 ? 'lineEven' : 'lineOdd'}>
            {showCode ? 
                <>
                    <td>{user.name} {user.surname}</td>
                    <td>{user.accessCode}</td>
                    <Validation test={showCode} securityLevel={user.securityLevel}/>
                </>
                :
                <>
                    <td className='unauthorized'>{user.name} {user.surname}</td>
                    <td className='unauthorized'>unauthorized level access </td>  
                    <Validation  test={showCode}/>
                </>  
                }
            </tr> 
        )
    })

    return (
        <table>
            <thead className='header'>
                <tr>
                    <td>Username</td>
                    <td>Access Code</td>
                    <td>Sec. Level</td>
                </tr>
            </thead>
            <tbody>
                {userCode}
            </tbody>
        </table>
    )
}
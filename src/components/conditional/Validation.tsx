interface Props {
    test: boolean,
    securityLevel?: string,
}

export default (props: Props) => {
    if(props.test) {
        return <td className='unauthorized'>{props.securityLevel}</td>
    } else {
        return <td className='unauthorized'></td>
    }
}
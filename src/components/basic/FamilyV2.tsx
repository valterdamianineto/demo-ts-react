import React, { cloneElement } from "react";

interface Props {
    name?: string;
    surname?: string;
    children: any
}

export default (props: Props) => {
    return (
        <>
            {props.children.map((child: React.ReactElement, i: number) => {
                return cloneElement(child, {...props, key: i});
                })
            }
        </>
    )
}
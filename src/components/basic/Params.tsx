interface Props {
    title: String,
    subtitle: String,
}

export default function Params(props: Props) {
    return (
        <div>
            <h1>{props.title}</h1>
            <h3>{props.subtitle}</h3>
        </div>
    )
}
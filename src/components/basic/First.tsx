function First() {
    const txt: String = 'First Rendered Component'
    return (
        <div>
            {txt}
        </div>
    )
}

export default First
import FamilyMember from "./FamilyMember"

interface Props {
    surname?: string
}

export default (props: Props) => {
    return (
        <>
            <FamilyMember name="Peter" {...props}/>
            <FamilyMember name="Elizabeth" {...props}/>
            <FamilyMember name="Neal" surname="Caffrey"/>
        </>
    )
}
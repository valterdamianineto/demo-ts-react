
interface Props {
    name?: string;
    surname?: string;
}
export default (props: Props) => {
    return (
        <>
            <p>{props.name} {props.surname}</p>
        </>
    )
}
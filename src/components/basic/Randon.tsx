interface Props {
    min: Number;
    max: Number;
}

export default (props: Props) => {
    const { min, max } = props

    const maxVal: String = 'The maximum value is ' + max
    const minVal: String = 'The minimum value is ' + min
    
    const randonNumber: Number = Math.floor(Math.random() * (max.valueOf() - min.valueOf() + 1 ) + min.valueOf())
    const result: String = ' The random value is ' + randonNumber

    return (
        <>
            <h2>The randon Selector</h2>
            <p>{maxVal}</p>
            <p>{minVal}</p>
            <p><strong>{ result }</strong></p>
        </>
    )
}
import "./Card.css";

interface Props {
    title: string;
    children: React.ReactNode;
    bgColor?: string;
    borderColor?: string;
    contentColor?: string;
}

export default  (props: Props) => {
    const cardStyle: React.CSSProperties = {
        backgroundColor: props.bgColor || 'silver',
        borderColor: props.borderColor || 'darkgray',
        color: props.contentColor || 'black'
    }
    return (
        <div className="card" style={cardStyle}>
            <div className="title" >{props.title}</div>
            <div className="content">{props.children}</div>
        </div>
    )
}
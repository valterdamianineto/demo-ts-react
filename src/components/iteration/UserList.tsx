import users from '../../data/user';
import "./UserList.css";
interface Props {

}

export default (props: Props) => {
    const user = users.map((user, i) => {
            return(
                <tr key={user.id} className={i % 2 === 0 ? 'userLineEven' : 'userLineOdd'}>
                    <td >
                        {user.name} {user.surname}
                    </td>
                    <td>
                        {user.securityLevel}
                    </td>
                </tr>
            )
        }
    )


    return (
        <table>
            <thead className='userHeader'>
                <tr>
                    <td>Username</td>
                    <td>Security Level</td>
                </tr>
            </thead>
            <tbody>
                {user}
            </tbody>
        </table> 
    )
}